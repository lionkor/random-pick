# random-pick

Randomly (and reproducibly) picks a candidate from a file with a list of candidates.

It uses `time(nullptr)` (unix time) to seed the PRNG, unless a seed is given via the commandline arguments.

## Usage

`./random-pick file.txt 346`

Will pick from file.txt with seed 346. The seed is optional and should only be specified to reproduce a previous roll.

## Randomness

It uses a mersenne prime twister, seeded with the seed or `time(nullptr)` to generate a uniform integer distribution across all indices of the array of candidates.
