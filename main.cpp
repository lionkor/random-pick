// Copyright (c) 2022 Lion Kortlepel
#include <cassert>
#include <ctime>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <random>

int main(int argc, char** argv) {
    if (argc < 2 || argc > 3) {
        std::cout << "Usage: " << argv[0] << " <file> [seed]" << std::endl;
        return 1;
    }
    size_t seed = time(nullptr);
    if (argc == 3) {
        seed = std::strtoull(argv[2], nullptr, 10);
    }
    std::cout << "using seed " << seed << std::endl;

    std::ifstream file(argv[1]);
    std::string line {};
    std::vector<std::string> lines {};
    std::cout << "got candidates: \n";
    while (std::getline(file, line)) {
        lines.push_back(line);
        std::cout << "\t" << line << "\n";
    }
    std::mt19937_64 mt; // mersenne twister
    mt.seed(seed);
    // between 0 (first candidate) and size-1 (last candidate)
    std::uniform_int_distribution<> distrib(0, lines.size() - 1);
    auto gen = distrib(mt);
    std::cout << "generated number: " << gen << std::endl;
    std::cout << "\nwinner: " << lines.at(gen) << std::endl;
}
